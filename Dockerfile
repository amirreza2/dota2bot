FROM nercury/cmake-cpp:latest
COPY src .
COPY CMakeLists.txt .
COPY lib .
RUN mkdir build && cd build && cmake .. && make
